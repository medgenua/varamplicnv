library(tidyr)

args <- commandArgs(TRUE)
allSampleMatFile <- args[[1]]
outfile <- args[[2]]
amp_count_data = read.table(allSampleMatFile,sep="\t")
ampMat <- spread(amp_count_data,V3,V2)
rownames(ampMat) <- ampMat$V1
ampMat <- ampMat[,-1]
ampMat <- as.matrix(ampMat)
#a1 = apply(ampMat,2,mean)
save(ampMat,file=outfile)
