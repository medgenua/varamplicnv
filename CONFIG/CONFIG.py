#!/usr/bin/python

import re,sys,os
import argparse

class CONFIG:

    def __init__(self,elements=[]):
        self.__elements={}
        for e in elements:
            self.__elements[e]=1

    def parseGCCommandArgs(self):

        cmdDict = {}

        parser = argparse.ArgumentParser(
                usage = '\npython getGCAmplicon.py '
                                '-a <amplicon-bed-file> -o <out-gc-file> '
                                '-b <two-bit-program> -f <two-bit-fasta>',
                description='Program to get GC content corresponding to amplicon coordinates'
        )
        parser.add_argument('--version', action='version', version='1.0') 
        parser.add_argument('-a','--ampliconFile',help='Position Sorted\
                            Amplicon design file',action='store',
                            dest='bedFile',required=True)
        parser.add_argument('-o','--outFile',help='Output file name',
                            dest='outFile',action='store',required=True)
        parser.add_argument('-b','--twoBitToFa',help='Binaries for converting\
                            two bit to Fasta',action='store',dest='twoBitToFa',
                            required=False)
        parser.add_argument('-f','--twoBitFasta',help='Bit format fasta sequence',
                            action='store',dest='twoBitFasta',required=True)

        #parser.add_argument('-v','--version',help='show program\'s version and exit\n\n\n',
        #                    action='version',version='%(prog)s 1.0')

	# required options
        self.results = parser.parse_args()
        self.bedFile = self.results.bedFile
        self.outFile = self.results.outFile
        #self.twoBit = self.results.twoBitToFa
        self.twoBitFasta = self.results.twoBitFasta
        cmd_dict = {'ampliconFile':self.bedFile,'outFile':self.outFile, 
                    'twoBitFasta':self.twoBitFasta
                }

        #optional options
        if self.results.twoBitToFa:
                self.twoBit =  self.results.twoBitToFa
                cmd_dict['twoBit'] = self.twoBit
        else:
                if os.system("which twoBitToFa") == 0:
                     cmd_dict['twoBit'] = 'twoBitToFa'
                else:
                     print("\n\nERROR: twoBitToFa binary not found in path. Please specify using --twoBitToFa option:\n\n")
                     parser.print_help()
                     sys.exit(2)
	
  

        print("\n\n")
        for key in cmd_dict.keys():
                print("Entered %s : %s" % (key,cmd_dict[key]))


        print("\n\n")

        return cmd_dict
    def parseMergeCountsCommandArgs(self):
        cmdDict = {}
        parser = argparse.ArgumentParser(
                usage = '\npython mergeCounts.py '
                                '-d <folder with count files> -o <output RData file> ',
                description='Merge data from individual count tasks into global data table'
        )
        parser.add_argument('--version', action='version', version='1.0') 
        parser.add_argument('-d','--DataDir',help='Folder containing count files from parseBam.py. All .txt files are considered valid input files!',action='store',
                            dest='DataDir',required=True)
        parser.add_argument('-o','--OutFile',help='Resulting RData file.',
                            action='store',dest='OutFile',required=False)
        #mandatory options
        self.results = parser.parse_args()
        self.DataDir = self.results.DataDir
        self.OutFile = self.results.OutFile
        cmd_dict = {'OutFile':self.OutFile,
                    'DataDir':self.DataDir}

        for key in cmd_dict.keys():
                print("Entered %s : %s" % (key,cmd_dict[key]))

        print("\n\n")

        return cmd_dict

    def parseRmDupAmpCommandArgs(self):

        cmdDict = {}

        parser = argparse.ArgumentParser(
                usage = '\npython processAmpliconRegion.py '
                                '-a <amplicon-bed-file> -s <ignore-bed-file> -r <gene_info-bed-file> -b <path_to_bedtools (optional)> -R <annotated_out_bedfile> -D <dedupped_out_bedfile>',
                description='Program to remove duplicate amplicon coordinates and annotate with gene information'
        )
        parser.add_argument('--version', action='version', version='1.0') 
        parser.add_argument('-a','--ampliconFile',help='Position Sorted\
                            Amplicon design file',action='store',
                            dest='bedFile',required=True)
        parser.add_argument('-s','--snpFile',help='Position Sorted\
                            SNP Amplicon design file', action='store',
                            dest='snpFile',required=False) 
        parser.add_argument('-r','--regionFile',help='Position Sorted\
                            ROI file. e.g. One line per Exon.',action='store',dest='regionFile',
                            required=True)
        parser.add_argument('-b','--bedTools',help='BedTools binaries',
                            action='store',dest='bedTools',required=False)
       	#parser.add_argument('-g','--gcOut',help='Output file with GC content',
	#                     action='store', dest='gcOut',required=True)
        parser.add_argument('-R','--roiOut',help='Annotated Region file',
                            action='store',dest='roiOut',required=True)
        parser.add_argument('-D','--dedupOut',help='Deduplicated amplicon list',
                            action='store' ,dest='dedupOut',required=True)
	#mandatory options
        self.results = parser.parse_args()
        self.bedFile = self.results.bedFile
        self.regFile = self.results.regionFile
        self.roiOut = self.results.roiOut
        self.dedupOut = self.results.dedupOut
        #self.outDir = self.results.outDir
        cmd_dict = {'ampliconFile':self.bedFile,
                    'regFile':self.regFile,
                    'roiOut':self.roiOut,
	            'dedupOut':self.dedupOut}

	# optional options
        if self.results.snpFile:
                self.snpFile = self.results.snpFile
                cmd_dict['snpFile'] = self.snpFile
		
        if self.results.bedTools:
                self.bedTools = self.results.bedTools
                cmd_dict['bedTools'] = self.bedTools
        else:
                # on path?  (test version)
                if os.system("bedtools --version") == 0:
                     cmd_dict['bedTools'] = 'bedtools'
                else:
                     print("\n\nERROR: bedtools binary not found in path. Please specify using --bedTools option:\n\n")
                     parser.print_help()
                     sys.exit(2)
	

        print("\n\n")
        for key in cmd_dict.keys():
                print("Entered %s : %s" % (key,cmd_dict[key]))

        print("\n\n")

        return cmd_dict

    def parseBAMCommandArgs(self):
        
        ''' Function to define command line arguments for processing BAM files '''
        cmdDict = {}
        parser = argparse.ArgumentParser(
                usage = '\npython processBAM.py '
                            '-i <input-BAM-directory> ' 
                            '-a <amplicon-bed-file> ' 
                            '-o <out-directory-path> '
                            '-b <analysis-batch-name> ',
                version = '1.0',
                description='Program to process BAM files and get read counts' 
                            'by mapping reads assigned uniquely to amplicons  '
        )
        parser.add_argument('-a','--ampliconFile',help='Position Sorted\
                            Amplicion design file',action='store',
                            dest='bedFile',required=True)
        parser.add_argument('-i','--inputBAM',help='Input directory of BAM\
                            files', action='store', dest='bamDir',required=True) 
        parser.add_argument('-o','--outDir',help='Output directory',
                            dest='outDir',action='store',required=True)
        parser.add_argument('-b','--batchNum',help='Analysis BATCH Name/Number',
                            dest='batchNum',action='store',required=True)
        self.results = parser.parse_args()
        self.bedFile = self.results.bedFile
        self.bamDir = os.path.abspath(self.results.bamDir)
        self.outDir = os.path.abspath(self.results.outDir)
        self.batch = self.results.batchNum

        cmd_dict = {'ampliconFile':self.bedFile,'outDir':self.outDir,
                    'bamDir':self.bamDir,'batch':self.batch}

        return cmd_dict
 
