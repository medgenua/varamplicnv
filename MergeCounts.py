#!/usr/bin/env python

import re,sys,os
from subprocess import check_call
import tempfile
global script_path
script_path = os.path.dirname(os.path.abspath(__file__))

sys.path.append(script_path+'/CONFIG/')

from CONFIG import *

if __name__=="__main__":
    #make a temp folder.
    tmpdirName = tempfile.mkdtemp()
    print(tmpdirName)
    # read config
    objC = CONFIG()
    cmd_dict = objC.parseMergeCountsCommandArgs()
    DataDir = cmd_dict['DataDir']
    OutFile = cmd_dict['OutFile']
    # open temporary outfile for merged data
    with open(tmpdirName+"/merged.txt","w") as fh:
        ## get files in DataDir
        for filename in os.listdir(DataDir):
            # append each valid file.
            if filename.endswith(".txt"):
                with open(DataDir+"/"+filename) as countfile:
                    fh.write(countfile.read())



    # reshape the data to the correct R format.
    check_call(["Rscript",script_path+"/BuildCountMAT.R",tmpdirName+"/merged.txt",OutFile])

    # clean up

    check_call(["rm","-Rf",tmpdirName])


