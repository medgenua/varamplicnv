# varAmpliCNV

varAmpliCNV is both an online/off-line tool for CNV calling from amplicon based targeted resequencing (TR) data enriched via Haloplex technologies. The highlighting features of the tool are:

  - Assigns reads directly to the amplicon coordinates to prune out aspecific alignments
  - Utilizes non-parametric model based on PCA/MDS to control the variance present in the data
  - Utilizes position specific dependencies between amplicons to filter out false positive CNV segments
  - Provides visualization plots to select CNV segments



### Requirements
 - R (>3.2.1 and <3.5.0 ), python 2.7
 - R packages: DNAcopy; ggplot2, Matrix, gtools
 - bedtools v2.28.0
 - All the BAM files
 - Amplicon design file (tab separated)
 - SNP-Amplicon-Gene File (tab separated)
 - Region of Interest (ROI) file annotated with exon number, gene name (tab separated)
 - Gender information per sample for every batches

It is recommended to use the docker version, available at : https://hub.docker.com/repository/docker/cmgantwerpen/varamplicnv

### Stage 1: Preprocessing step 
Preprocessing should be done once and can be re-used in follow-up projects. 

#### 1. Processing input Amplicon design file

First, remove SNP (unwanted amplicons) coordinates and duplicate coordinates. Also adding Region number to input region of interest (ROI) design file. 

```sh
$ python processAmpliconRegion.py \
  -a  <RAW-amplicon-bed-file> \
  -s <snp-amplicon-file> \
  -r <ROI-design-file> \
  -R <path-to-ANNOTATED-BED> \
  -D <path-to-DEDUP-BED-out>
```

**Example:**
 
```sh
$ python processAmpliconRegion.py \
  -a /home/user/BED/37328-1448381652_Amplicons.bed \
  -s /home/user/BED/snpAmpGene.bed \
  -r /home/user/BED/37328-1448381652_GeneInfo.bed \
  -R /home/user/BED/37328-1448381652_annotated.bed \
  -D /home/user/BED/37328-1448381652_dedupped.bed
```

**Notes:**

 * optional parameter : -b : path to bedtools (if not in $PATH)
 * optional parameter : -s : bed files of amplicons to exclude from CNV analysis (e.g. sample tracking snps)

#### 2. Getting GC content per amplicon
	
```sh
$ python getGCAmplicon.py \
  -a <path-to-amplicon-bed-file> \
  -o <path-to-GCContent-output-file> \
  -b <two-bit-binary> \
  -f <two-bit-fasta-file>
```

**Example:**

```sh 
$ python getGCAmplicon.py \
  -a /home/user/BED/37328-1448381652_dedupped.bed \
  -o /home/user/BED/GCContent.csv \
  -b /opt/binaries/twoBitToFa/default/twoBitToFa \
  -f /opt/References/hg19/2bit/hg19.2bit 
```

**Notes:**

 * optional parameter : -b : twoBitToFa is taken from $PATH if not specified. 

### Stage 2: Assigning reads uniquely to Amplicons

The BAM files are parsed to retrieve reads and assign uniquely to the amplicons, based on exact matching to their start and end coordinates

```sh
$ python parseBAM.py \
  <path-to-DEDUP-bedfile> \
  <path-to-BAM-file> \
  <path-to-outfile-COUNTS> \
  <path-to-outfile-UNMAPPED_BAM> \
  <path-to-outfile-STATS>
```

**Example:**
```sh
$ python parseBAM.py \
  /home/user/BED/37328-1448381652_dedupped.bed \
  /home/user/BAM/sample1.bam \
  /home/user/COUNTS/sample1.txt \
  /home/user/UNMAPPED/sample1.bam \
  /home/user/STATS/sample1.txt
```  

**Example:** Parallel processing of all BAMs in folder:
```sh
$ parallel python parseBAM.py \
  /data_dir/test_varAmpliCNV/BED/testAmpRegGeneTAAD_FINAL.dedup.bed \
  {} \
  /data_dir/test_varAmpliCNV/COUNTS/{/.}.txt \
  /data_dir/test_varAmpliCNV/UNMAPPED/{/.}.bam \
  /data_dir/test_varAmpliCNV/STATS/{/.}.txt \
  ::: /data_dir/test_varAmpliCNV/BAM_Files/*bam
``` 


### Stage 3: Predicting  CNVs 

#### 1. Merging counts

CNVs are predicted on batches of samples. It is recommended to use run-based batches, but any number of count files can be merged into a single datafile using the command below: 

```sh
python MergeCounts.py  \
  -d <path-to-count-files-dir> \
  -o <path-to-outfile.RData>
```

**Example:**
```sh
python MergeCounts.py \
  -d /home/user/COUNTS/ \
  -o /home/user/COUNTS/AllCounts.RData
```

#### 2. Calling CNVs

```sh
$ Rscript varAmpliCNV.R \
  -i <sample-amplicon-counts.RData> \
  -o <output-directory> \
  -b <dedup-bed-file> \
  -c <gc-content-file> \
  -r <ROI-bed-file> \
  -s <gender-file> \
  -p <proprtion-of-variance for Auto/Sex: default:0.80> \
  -n <DS=>0 or AOF=>1 Flag; default:1>
  -d <logR Threshold for deletions : default -0.2 (AOF); -0.5 (DS)>
  -D <logR Threshold for duplications: default 0.38 (AOF); 0.5 (DS)>
```

**Example:**
```sh
$ Rscript varAmpliCNV.R \
    -i /home/user/COUNTS/AllCounts.RData \
    -o /home/user/OUTPUT \
    -b /home/user/BED/37328-1448381652_dedupped.bed \
    -r /home/user/BED/37328-1448381652_annotated.bed \
    -c /home/user/BED/GCContent.csv \
    -s /home/user/META/Sample_Gender.txt \
    -p 0.80  \
    -n 1
    -d -0.2
    -D 0.38
```

**Notes:**

 * parameter : -p : fraction of variance to remove from data.
 * parameter : -n : Post-process calls using amplicon-overlap filtering (0/1). Plotting is only activated if "-n 1" is used.
 * parameter : -s : gender details to generate sex-specific reference for X/Y. Format is : <sample>\t<M/F/U>. 
 * parameterss : -d/-D : Control minimal signal strength to filter CNVs


**Output files:**

 * Filtered.Segments.pdf  : CNV-plots for retained variants (if -n == 1)
 * parameter_settings.txt : set & derived settings + discarded samples
 * Quality_Measures.pdf   : Quality Plots : coverage, variance
 * Segments.Full.txt      : Segmentation results without filtering.
 * Segments.Filtered.txt  : Filtered Segmentation results.  


### Things To Do

 - Add little more documentation to the code/functions
 - Documentation explaining input file format types related to Amplicon design files.


