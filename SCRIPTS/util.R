getCommandOptionCheck = function(opt,opt_parser) {
    
    if (is.null(opt$ampCountMat)){
        print_help(opt_parser)
        stop("Amplicon count matrix R-data is missing\n",call.=FALSE)
    }
    else if (is.null(opt$bedFile)){
        print_help(opt_parser)
        stop("Amplicon bed file missing\n",call.=FALSE)
    }
    else if (is.null(opt$roi)){
        print_help(opt_parser)
        stop("ROI file is missing\n",call.=FALSE)
    }
    else if (is.null(opt$gcContent)){
        print_help(opt_parser)
        stop("GC-Content file is missing\n",call.=FALSE)
    }
    # optional arguments
    if (is.null(opt$gender)){
        cat("\nSample-Gender file is missing : X-Calling will be sub-optimal\n")
    	cat("   => Requireded format (tab separated) : <sample_name>   M/F/U\n\n")
    }
    if (is.null(opt$propVarFrac)){
        cat("Proprtion of Variance is missing\n")
        cat("Setting to default of 0.8")
        opt$propVarFrac = 0.8
    }
    if (is.null(opt$analysis)){
        cat("DS/AOF flag is missing\n")
        cat("Setting to AOF")
        opt$analysis = 1
    }
    # thresholds ? 
    if (is.null(opt$del)) {
        if (opt$analysis == 1) {
            opt$del = -0.2
        }
        else {
            opt$del = -0.5
        }
    }
    if (is.null(opt$dup)) {
        if (opt$analysis == 1) {
            opt$dup = 0.38
        }
        else {
            opt$dup = 0.5
        }
    }
    return(opt)

}


createFolders <- function(out_dir) {
    
    out_anal_dir = paste(out_dir,"/Samples/",sep="")
    out_plot_dir = paste(out_dir,"/General/",sep="")
    system(paste("mkdir -p ",out_anal_dir,sep=""))
    system(paste("mkdir -p ",out_plot_dir,sep=""))

    dir_list = list()
    dir_list = list(out_anal_dir,out_plot_dir)

    return(dir_list)

}
