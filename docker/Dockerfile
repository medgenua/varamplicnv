############################################################
# Dockerfile to build varAmpliCNV image
############################################################

# Set the base image to Ubuntu
FROM ubuntu:18.04

# Image creator
LABEL author="geert vandeweyer <geert.vandeweyer@uza.be>"

# Software information
LABEL software="varAmpliCNV"
LABEL software.version="1.0"
LABEL about.home="doi:to-add"

## what are the apt packages we need:
ARG BUILD_PACKAGES="wget git "
## what are the conda packages we need:
ARG CONDA_PACKAGES="R=3.6 r-ggplot2=3.3.2 r-matrix=1.2 r-gtools=3.8.2 bioconductor-dnacopy=1.60.0 samtools=1.7 bedtools=2.28.0 ucsc-twobittofa pysam=0.15.3 r-optparse=1.6.6 r-r.utils=2.9.2 r-gridextra=2.3 r-gplots=3.0.4 r-tidyr=1.1.1 xorg-libxrender=0.9.10 cairo=1.14.12 fonts-conda-ecosystem=1 r-rlist=0.4.6.1 r-pdftools=2.2 parallel=20200722"
##  ENV SETTINGS during runtime
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH=/opt/conda/bin:/opt/varAmpliCNV/:$PATH

RUN apt-get update && \
    # essential dependencies
    apt-get install -y $BUILD_PACKAGES && \
    ## python DEPENDENCIES using miniconda for python2
    wget --quiet https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -u -p /opt/conda && \
    /opt/conda/bin/conda config --add channels defaults && \
    /opt/conda/bin/conda config --add channels bioconda && \
    /opt/conda/bin/conda config --add channels conda-forge && \
    # install conda packages. 
    /opt/conda/bin/conda install -y $CONDA_PACKAGES && \
    # clean up
    rm ~/miniconda.sh  && \
    /opt/conda/bin/conda clean -tipsy && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    ## install varampliCNV scripts.
    #mkdir -p /opt/varAmpliCNV && \
    git clone https://bitbucket.org/medgenua/varamplicnv.git /opt/varAmpliCNV && \
    # get mdcat 
    cd /opt && \ 
    wget 'https://github.com/lunaryorn/mdcat/releases/download/mdcat-0.21.1/mdcat-0.21.1-x86_64-unknown-linux-musl.tar.gz' && \
    tar xzvf mdcat-0.21.1-x86_64-unknown-linux-musl.tar.gz && \
    ln -s /opt/mdcat-0.21.1-x86_64-unknown-linux-musl/mdcat /usr/bin/ && \
    rm -f /opt/mdcat-0.21.1-x86_64-unknown-linux-musl.tar.gz

CMD mdcat /opt/varAmpliCNV/README.md




