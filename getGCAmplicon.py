#!/usr/bin/env python

import re,sys,os
from subprocess import check_call
import tempfile
global script_path
script_path = os.path.dirname(os.path.abspath(__file__))

sys.path.append(script_path+'/CONFIG/')

from CONFIG import *

if __name__=="__main__":
		#make a temp folder.
		tmpdirName = tempfile.mkdtemp()
		# read config
		objC = CONFIG()
		cmd_dict = objC.parseGCCommandArgs()
		bed_file = cmd_dict['ampliconFile'] 
		out_file = cmd_dict['outFile']
		two_bit = cmd_dict['twoBit']
		two_bit_fa = cmd_dict['twoBitFasta']

		fh = open(bed_file)

		bit_in = open(tmpdirName+"/bit_infile.txt","w")
		for lines in fh:
			lines = lines.strip()
			if not re.search("^browser|^track",lines):
				strs = re.split("\t",lines)
				chr_num = strs[0].strip()
				chr_st = int(strs[1].strip())+1
				chr_end = int(strs[2].strip())
				amp_id = strs[3].strip()
				bit_in.write(chr_num+":"+str(chr_st)+"-"+str(chr_end)+"\n")
		
		bit_in.close()
		fh.close()

		#print("Extracting sequences")
		command = two_bit+" "+two_bit_fa+"  -noMask -seqList="+tmpdirName+"/bit_infile.txt "+tmpdirName+"/all.regions.fa"
		#print command
		check_call(command,shell=True)
		#print("Parsing sequences");
		fh2 = open(tmpdirName+"/all.regions.fa")
		gc_count =0
		dna_list = [] 
		rname = ''
		gc_values = dict() 
		for lines in fh2:
			lines = lines.strip()
			if re.search(">",lines):
				# something stored? 
				if len(dna_list):
					count_g = "".join(dna_list).count("G")
					count_c = "".join(dna_list).count("C")
					gc_count = count_g+count_c
					gc_values[rname] = [count_g]+[count_c]+[gc_count]
					#print_list = [chr_num]+[chr_st-1]+[chr_end]+[amp_id]+[count_g]+[count_c]+[gc_count]
				dna_list = []
				rname = lines[1:]
			else:
				dna_list.append(lines)
				gc_count = gc_count+1
				
		if len(dna_list):
			count_g = "".join(dna_list).count("G")
			count_c = "".join(dna_list).count("C")
			gc_count = count_g+count_c
			gc_values[rname] = [count_g]+[count_c]+[gc_count]

		fh2.close()
		#print("Writing output file")
		fh = open(bed_file)
		wh = open(out_file,"w")
		wh.write("CHR,CHR_ST,CHR_EN,AMP_ID,G_Count,C_Count,GC_Count\n")

		for lines in fh:
			lines = lines.strip()
			if not re.search("^browser|^track",lines):
				strs = re.split("\t",lines)
				chr_num = strs[0].strip()
				chr_st = int(strs[1].strip())+1
				chr_end = int(strs[2].strip())
				amp_id = strs[3].strip()
				rname = chr_num+":"+str(chr_st)+"-"+str(chr_end)
				print_list = [chr_num]+[chr_st-1]+[chr_end]+[amp_id]+gc_values[rname]
				wh.write(",".join(["%s" % x for x in print_list])+"\n")
		fh.close()
		wh.close()
		# clean up
		check_call(["rm","-Rf",tmpdirName])
				

