#!/usr/bin/env python

import re,sys
import pysam
import tempfile
from subprocess import check_call
#import subprocess
import os
#import operator
#import csv

def getBedHash(bed_file):

	fh1 = open(bed_file)
	amplicon_counts = {}
	
	for lines in fh1:
                lines = lines.strip()
                if not re.search("^browser|^track",lines):
                        strs = re.split("\t",lines)
                        chr_str = strs[0]+"_"+str(strs[1])+"_"+str(strs[2])
                        amp_id = strs[3]
                        amplicon_counts[amp_id] = 0
		
	fh1.close()
	
	return amplicon_counts

if __name__=="__main__":

	bed_file = os.path.abspath(sys.argv[1])
	bam_file = os.path.abspath(sys.argv[2])
	out_counts = os.path.abspath(sys.argv[3])
	out_unmapped = os.path.abspath(sys.argv[4])
	out_status = os.path.abspath(sys.argv[5])
        tmpdirName = tempfile.mkdtemp()

        print bed_file,"\n",bam_file,"\n",out_counts,"\n",out_status,"\n",tmpdirName,"\n"
	
	# read bed file 
	fh1 = open(bed_file)
	bed_hash = {}
	amplicon_by_start = {}
	amplicon_by_end = {}
	doubles_hash = {}
	amplicon_counts = {}
	
	for lines in fh1:
		lines = lines.strip()
		if not re.search("^browser|^track",lines):
			strs = re.split("\t",lines)
			chr_str = strs[0]+"_"+str(strs[1])+"_"+str(strs[2])
			amp_id = strs[3]
			bed_hash[chr_str] = amp_id

			# store by start position.
			if not str(strs[0]) in amplicon_by_start: #.has_key(str(strs[0])):
				amplicon_by_start[str(strs[0])] = {}
			if not int(strs[1]) in amplicon_by_start[str(strs[0])]: #.has_key(int(strs[1])):
				amplicon_by_start[str(strs[0])][int(strs[1])] = {}
			if int(strs[2]) in amplicon_by_start[str(strs[0])][int(strs[1])]: #.has_key(int(strs[2])):
				doubles_hash[amp_id] = chr_str
				continue
			amplicon_by_start[str(strs[0])][int(strs[1])][int(strs[2])] = amp_id
			# store by end position.
			if not str(strs[0]) in amplicon_by_end: #.has_key(str(strs[0])):
				amplicon_by_end[str(strs[0])] = {}
			if not int(strs[2]) in amplicon_by_end[str(strs[0])]: #.has_key(int(strs[2])):
				amplicon_by_end[str(strs[0])][int(strs[2])] = {}
			amplicon_by_end[str(strs[0])][int(strs[2])][int(strs[1])] = amp_id

			# counter
			amplicon_counts[amp_id] = 0


	fh1.close()

	bam_hash = {}
	
	amplicon_counts = getBedHash(bed_file)
	
	bam = pysam.AlignmentFile(bam_file,"rb")
	sample_id = (bam.header['RG'][0]['SM'])
	out_bam = pysam.AlignmentFile(tmpdirName+'/unsorted.bam',"wb",template=bam)
	read_hash = {}


	# go through BAM using pysam.
	#bam_file = pysam.AlignmentFile(sample_str,"rb")
	exact_count = fuzzy_count = no_match = unmapped = align_problems = 0
	for read in bam.fetch():
		# skip unmapped reads (or mate unmapped):
		if read.is_unmapped or read.mate_is_unmapped:
			unmapped += 1
			continue

		# some abnormal alignments : invalid chromosome names
		if read.reference_id < 0 or read.next_reference_id < 0: 
			align_problems += 1
			continue
		# some abnormal alignments : different chromosomes.
		if read.reference_name != read.next_reference_name:
			align_problems += 1
			continue

		# no probes on this chromosome.
		chr_txt = str(read.reference_name)
		if not chr_txt in amplicon_by_start: #.has_key(chr_txt):
			align_problems += 1
			continue;

		
		start = read.reference_start
		end = read.reference_end

		# this is the first read: cache.
		if not read.query_name in read_hash: #.has_key(read.query_name):
			read_hash[read.query_name] = read
			continue


		# this is the mate : process.
		mate_start = read_hash[read.query_name].reference_start
		mate_end = read_hash[read.query_name].reference_end

		# some abnormal alignments : same orientation.
		if read.is_reverse == read_hash[read.query_name].is_reverse:
			align_problems += 1
			read_hash.pop(read.query_name,None)
			continue



		min_start = min(start,mate_start)
		max_end = max(end,mate_end)
		## matches to amplicon (exact)?
		if min_start in amplicon_by_start[chr_txt] and max_end in amplicon_by_start[chr_txt][min_start] :
			amp_id = amplicon_by_start[chr_txt][min_start][max_end]
			amplicon_counts[amp_id] += 1
			exact_count += 1
			read_hash.pop(read.query_name,None)


		else:
			# do fuzzy matching : todo.
			
			out_bam.write(read)
			out_bam.write(read_hash[read.query_name])
			read_hash.pop(read.query_name,None)
			no_match += 1
			
		
					


	bam.close()
	out_bam.close()
	# sort the umapped bam.
	pysam.sort("-o",out_unmapped,tmpdirName+"/unsorted.bam")

	# write stats.
	wh1 = open(out_status,"w")
	wh1.write(bam_file+"\n")
	wh1.write("Sample Name : %s\n" % sample_id)
	wh1.write("Total Reads: %s\n" % (exact_count + fuzzy_count + no_match + unmapped + align_problems))
	wh1.write("exact match : %s\n" % exact_count)
	#print >>wh1,"fuzzy match :",fuzzy_count
	wh1.write("No Match : %s\n" % no_match)
	wh1.write("Not Mapped: %s\n" % unmapped)	
	wh1.write("Abnormal Alignments: %s\n" % align_problems)
	wh1.close()
	wh2 = open(out_counts,'w')
	#wh2.write("Amplicon\t%s\n" % sample_id)
	for keys in amplicon_counts:
		wh2.write("%s\t%s\t%s\n" % (keys, amplicon_counts[keys],sample_id))

	wh2.close()
        check_call(["rm","-Rf",tmpdirName])

