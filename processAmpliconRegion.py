#!/usr/bin/env python

import re,sys,os
from subprocess import check_call
import tempfile
from collections import OrderedDict

global script_path
script_path = os.path.dirname(os.path.abspath(__file__))

sys.path.append(script_path+'/CONFIG/')

from CONFIG import *

if __name__=="__main__":

	objC = CONFIG()
	cmd_dict = objC.parseRmDupAmpCommandArgs()

	amp_file = cmd_dict['ampliconFile']
	reg_file = cmd_dict['regFile']
	#out_dir = cmd_dict['outDir']
	bed_tools = cmd_dict['bedTools']
	roiOut = cmd_dict['roiOut']
	dedupOut = cmd_dict['dedupOut']

	# temporary directory:
	tmpdirName = tempfile.mkdtemp()
	# Processing amplicon design file
	amp_hash = OrderedDict()
	fh = open(amp_file)

        for lines in fh:	
			lines = lines.strip()
			if not re.search("^browser|track",lines):
				strs = re.split("\t",lines); strs = [x.strip() for x in strs ]
				chr_num = strs[0]
				chr_st = strs[1]
				chr_en = strs[2]
				amp_id = strs[3]
				key_str = chr_num+"_"+str(chr_st)+"_"+str(chr_en)
		
				if key_str in amp_hash: #.has_key(key_str):
					tmp = amp_hash[key_str]
					tmp.append(amp_id)
					amp_hash[key_str] = tmp
				else:	
					amp_hash[key_str] = [amp_id]
	fh.close()
	
	#Processing snp-amplicon design file, if specified.
	if 'snpFile' in cmd_dict:
			snp_file = cmd_dict['snpFile']
	
			snp_hash = {}
			fh = open(snp_file)
	
			for lines in fh:
				lines = lines.strip()
			
				if not re.search('^browser|^track',lines):
					strs = re.split('\t',lines); strs = [x.strip() for x in strs]
					chr_num = strs[0]
					st_coord = strs[1]
					en_coord = strs[2]
					key_str = chr_num+"_"+st_coord+"_"+en_coord
					snp_hash[key_str] = 1
			fh.close()
			# Output the amplicon coordinates without SNPs
			for keys in amp_hash:
				if keys in snp_hash: #.has_key(keys):
					del amp_hash[keys]
					
		
	# Output the amplicon design file after filtering for SNPs
	tmp_file = tmpdirName+'/tmp_AmpRmSNPDup.bed'
	out_file = dedupOut #tmpDirName+'/AmpRmSNPRmDup.bed'
	
	wh = open(tmp_file,'w')

	for keys in amp_hash:
			strs = re.split('\_',keys)
			# keep a single amplicon name per duplicate.
			wh.write('\t'.join(strs)+'\t'+amp_hash[keys][0]+"\n")
	wh.close()
	
	# Sort the amplicon design file obtained in previous step
	sort_cmd = 'sort -n -k1.4 -k2,2n '+tmp_file+' > '+out_file
	check_call(sort_cmd,shell=True)
	
	# Add regions to ROI file
	tmp_reg_file = tmpdirName+'/tmpGeneRegion.bed'
	sort_reg_gene_file = tmpdirName+'/tmpSortROI.bed'
	merge_reg_gene_file = tmpdirName+'/tmpSortMergeROI.bed'
	sort_merge_gene_region_file = roiOut #tmpdirName+'/sortMergeROIGene.bed'
	
	# Processing Input region file
	fh = open(reg_file)
	wh = open(tmp_reg_file,'w')
	
	for lines in fh:
			lines = lines.strip()
			if not re.search('^browser|^track',lines):
				strs = re.split("\t",lines);strs = [x.strip() for x in strs]
				wh.write('\t'.join([strs[0],strs[1],strs[2],strs[3]])+"\n")
	wh.close()
	fh.close()
	
	sort_cmd = 'sort -V -k1,1 -k2,2n '+tmp_reg_file+' > '+sort_reg_gene_file
	merge_cmd = bed_tools+' merge -i '+sort_reg_gene_file+' -c 4 -o distinct > '+merge_reg_gene_file
	check_call(sort_cmd,shell=True)
	check_call(merge_cmd,shell=True)
	
	fh = open(merge_reg_gene_file)
	
	gene_exons = OrderedDict()
	
	# Start processing the Gene-Region file and using hasing technique to store the coordinated per Gene
	for lines in fh:
			lines = lines.strip()
			strs = re.split('\t',lines); strs = [x.strip() for x in strs]
			gene_id = strs[3]
			if gene_id in gene_exons: #.has_key(gene_id):
				tmp = gene_exons[gene_id]
				tmp.append(lines)
				gene_exons[gene_id] = tmp
			else:
				gene_exons[gene_id] = [lines]
	
	fh.close()
		
	# Iterate through the Gene-Exon dictionary to add sequentially the Regions to each of these coordinates of Region-Of-Interest
	wh = open(sort_merge_gene_region_file,'w')
	for keys in gene_exons:
                        exon_length = len(gene_exons[keys])
			for i in range(0,exon_length):
				strs = re.split('\t',gene_exons[keys][i])
				wh.write(strs[0].strip()+"\t"+str(strs[1].strip())+"\t"+str(strs[2].strip())+"\t"+strs[3].strip()+"|Region_"+str(i)+"\n")
	wh.close()
	
        cmd="rm -Rf %s" % (tmpdirName)
        check_call(cmd,shell=True)
	#os.system('rm '+tmp_reg_file)
	#os.system('rm '+sort_reg_gene_file)
	#os.system('rm '+merge_reg_gene_file)
	
		
	
